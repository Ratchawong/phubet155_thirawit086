
<?php require_once('auth.php');?>
 <?php
 include('../connect2.php');
?>   
    
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php include('h.php');?>
    <?php include('datatable.php');?>

  </head>
  <body>
  <div class="container">
  <div class="row">
         <?php include('banner.php');?>
   </div>
    <div class="row">
     <div class="col-md-12">
          <?php include('navbar.php');?>
        </div>
      <div class="col-md-2">
        
        <?php include('menu.php');?>           
      </div>
 
    <div class="col-md-10">
      <div class="panel panel-primary class">
    
          <div class="panel-heading" align="center" style="font-size: 20px;">รายการที่ต้องชำระ</div>
        
           <div class="panel-body ">

   <table class="table-bordered " width="100%" >
   <?php  
// ติดต่อฐานข้อมูล 
 include('../connect.php'); 
   $id = $_GET['id'];
    $adid = $_GET['adid'];
    $payid = $_GET['payid'];
    $result = $db->prepare("SELECT * FROM `order`,`payment` WHERE  order.or_id = payment.or_id 
    and payment.or_id = :adminid and payment.admin_id=:adminid2 and payment.pay_id = :adminid3 ");
    $result->bindParam(':adminid', $id);
    $result->bindParam(':adminid2', $adid);
    $result->bindParam(':adminid3', $payid);
    $result->execute();
    for($i=0; $rs = $result->fetch(); $i++){   
    
    ?>
    
 <!-- Content Row -->
              
     <div class="panel-body " >    

<form  action="InsertPayment.php" name="frmAdd" id="frmAdd" method="post" enctype="multipart/form-data">

  <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td colspan="3" align="center">&nbsp;</td>
    </tr>
<div hidden="">
    <label class="col-sm-3 control-label"> รหัส :  <span class="starrequired">*</span></label>
          <div class="col-sm-9"><input class="form-control" type="text" value="<?php echo($rs['pay_id'])?>" name="pay_id"  /readonly><br> </div>
</div>
 <div hidden="">
    <label class="col-sm-3 control-label"> รหัสลูกค้า :  <span class="starrequired">*</span></label>
          <div class="col-sm-9"><input class="form-control" type="text" value="<?php echo($rs['admin_id'])?>" name="admin_id"  /readonly><br> </div>
</div>
 <div hidden="">         
   <label class="col-sm-3 control-label"> รหัสินค้า :  <span class="starrequired">*</span></label>
          <div class="col-sm-9"><input class="form-control" type="text" value="<?php echo($rs['or_id'])?>" name="or_id"  /readonly><br> </div>
  
        <tr >
</div>

             
          <label class="col-sm-3 control-label"> ชื่อ-สกุล :  <span class="starrequired">*</span></label>
          <div class="col-sm-9"><input class="form-control" type="text" value="<?php echo($rs['name'])?>" name="name"  /readonly><br> </div> 
          
        <label class="col-sm-3 control-label"> ราคา :  <span class="starrequired">*</span></label>
          <div class="col-sm-9"><input class="form-control" value="<?php echo($rs['total'])?>" 
            name="pay_total"  /readonly> <br> </div>


 <div hidden="">            
     <label class="col-sm-3 control-label"> สถานะ :  <span class="starrequired">*</span></label>
       <div class="col-sm-9"><input class="form-control" name="pay_status_bank" value="ชำระแล้ว"  /readonly> <br> </div>
</div>
 <div hidden="">
   <label class="col-sm-3 control-label"> วันที่โอน :  <span class="starrequired">*</span></label>
       <div class="col-sm-9"><input class="form-control" value="วันนี้" name="pay_time"  /readonly> <br> </div>
</div>
 <div hidden="">
  <label class="col-sm-3 control-label"> ธนาคาร :  <span class="starrequired">*</span></label>
       <div class="col-sm-9"><input class="form-control" value="กรุงไทย" name="pay_bank"  /readonly> <br> </div>
  </div>
 
  <label class="col-sm-3 control-label">รูปสลิป :  <span class="starrequired">*</span></label>
  <div class="col-sm-9"><input type="file"  class="form-control"  name="pay_pic"  /> <br> </div>
        
       
        <br>  </label></div>   

  <div class="form-group">
      <div class="col-sm-offset-3 col-sm-9">
        <button class="btn btn-primary" type="submit"  name="btn-upload">ตกลง</button>
    <button  type="reset" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
    
      </div>
    </div>
     
</div> 
    </table>
 
</form>
        <br>
    </tr>
   
      <?php }
      ?>         
            </table>   
            
             
       </tbody>
         </div>
         </div>
         </div>
                        </div>
 </div> 
</form>
 
</body>
</html>