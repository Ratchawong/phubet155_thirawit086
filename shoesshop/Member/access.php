<?php
	//Start session
	session_start();
	
	//Check whether the session variable SESS_MEMBER_ID is present or not
	//ตรวจสอบ SESS_MEMBER_ID
	if(!isset($_SESSION['u_id']) || (trim($_SESSION['u_id']) == '')) {
		header("location: ../index.php");
		exit();
	}
?>