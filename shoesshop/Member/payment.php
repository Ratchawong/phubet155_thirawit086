
<?php require_once('auth.php');?>
 <?php
 include('../connect2.php');
?>   
		
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<?php include('h.php');?>
    <?php include('datatable.php');?>

  </head>
  <body>
  <div class="container">
  <div class="row">
         <?php include('banner.php');?>
   </div>
	  <div class="row">
		 <div class="col-md-12">
          <?php include('navbar.php');?>
        </div>
      <div class="col-md-2">
        
        <?php include('menu.php');?>        	 
      </div>
 
    <div class="col-md-10">
      <div class="panel panel-primary class">
    
          <div class="panel-heading" align="center" style="font-size: 20px;">รายการที่ต้องชำระ</div>
        
           <div class="panel-body ">

   <table class="table-bordered " width="100%" >
          

    
 <!-- Content Row -->
     
              
            </div>       
         <table class="table table-bordered" id="resultTable" data-responsive="table" style="text-align: center;">
  <thead>
        <tr align="center">
              <td hidden="">วันที่สั่ง</td>
                        <td>รหัสการสั่งซื้อ</td>
            <td>ราคาราคาสุทธิ์</td>
                        <td>สถานะชำระเงินุ</td>
                       
                <td>หมายเหตุ</td>
                        
                        
                        
        </tr>
  
</thead>
  <tbody>  
  <?php
   include('../connect1.php');
  
    $sql="SELECT DISTINCT (payment.or_id),(payment.pay_id),(order.or_id),(order.total),(payment.pay_status_bank) 
    FROM `order`,`payment`,`admin` 
    WHERE payment.or_id = order.or_id 
    AND payment.admin_id = admin.admin_id 
    AND pay_status_bank ='ยังไม่ชำระ'
    AND admin.admin_id = '".$_SESSION['admin_id']."' order by payment.pay_id desc"  ;
    
    $result=mysqli_query($conn,$sql);
    while($data=mysqli_fetch_array($result, MYSQLI_ASSOC))
    { 
    ?>
      <tr class="record">
      
            <td hidden=""><?php echo $data['pay_id'] ;?> </td>
            <td hidden=""><?php echo $data['date'] ;?> </td>
            <td><?php echo $data['or_id'];?> </td> 
            <td> <strong><?php echo $data['total'];?> </strong></td> 
            <td style="color:#060"><strong><?php echo $data['pay_status_bank'];?></strong> </td>  
          
      <td>                      
      <a href="informPayment.php?id=<?php echo $data["or_id"];?> &adid=<?php echo "".$_SESSION["admin_id"].""; ?> &payid=<?php echo $data["pay_id"]; ?> "class="btn btn-primary"><i class="fa fa-shopping-cart"></i> แจ้งชำระสินค้า</a></center>
      <a href="del_payment.php?id=<?php echo $data["or_id"];?> &adid=<?php echo "".$_SESSION["admin_id"].""; ?> &payid=<?php echo $data["pay_id"]; ?> "class="btn btn-primary"><i class="fa fa-shopping-cart"></i> ลบ</a></center> </td>
        
       
      <?php  
      }
       
      ?>
        <br>
    </tr>
   
               
            </table>   
          
            </td>       
       </tbody>
         </div>
         </div>
         </div>
                        </div>
 </div> 
</form>
 
</body>
</html>