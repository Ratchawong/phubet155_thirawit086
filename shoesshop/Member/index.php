<?php require_once('auth.php');?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<?php include('h.php');?>
    <?php include('datatable.php');?>

  </head>
  <body>
  <div class="container">
  <div class="row">
         <?php include('banner.php');?>
   </div>
	  <div class="row">
		  <div class="col-md-12">
          <?php include('navbar.php');?>
        </div>
      <div class="col-md-2">
        
        <?php include('menu.php');?>        	 
      </div>
      <div class="col-md-10">
		  <div class="panel panel-primary class">
			<?php 
			    include('../connect.php');
				$result = $db->prepare("SELECT * FROM product WHERE p_id");
				$result->execute();
				$row = $result->rowcount();

			?>
        	<div class="panel-heading" align="center" style="font-size: 20px;">สินค้าทั้งหมด<font color="#FDFC06" style="font:bold 20px 'Aleo';">[<?php echo $row;?>] รายการ </font> </div>
			  
           <div class="panel-body ">
	<!-- ค้นหา -->
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script>    
		$(document).ready(function() {
			$('#example').DataTable();

		} );
	</script>
          <table class="table-bordered " width="100%"  id='example'  >
			 <thead>  
	 
          <tr>
		<th>ลำดับ</th>
        <th >สินค้า</th>
        <th>รูปสินค้า</th>
        <th>Size</th>
		<th>ราคา</th>
		<th>หยิบใส่ตะกร้า</th>
            
          </tr>
        </thead>
         <?php
								include('../connect1.php');
							  
								$sql= "select * from product order by p_id  ";
								$result=mysqli_query($conn,$sql);
							//	for($i=0; $data=$result->fetch(); $i++ );{
	 									$j=1;
									
								while($data=mysqli_fetch_array($result, MYSQLI_ASSOC)){
								
							  ?>
            <tr>
                <td align="center" valign="top"><?php echo($j) ?></td>
      
               </td>

		<td><?php echo $data['p_name']; ?></td> 
		<?php foreach(range(1,1) as $i):?>
          <td><center> <img src="../img/<?php echo $data["p_pic"] ?>" width="34" height="35" class="imgx"></center></td>
		<?php endforeach;?>
		  <td><?php echo $data['p_size']; ?></td> 
		  <td><?php echo $data['p_price']; ?></td> 

          <td valign="top"><a href="checkorder.php?ProductID=<?php echo "".$data["p_id"]."" ;?>" class="btn btn-info btn-xs glyphicon lyphicon glyphicon-shopping-cart"> หยิบใส่ตะกร้า </a></td>
            </tr>
            <?php	$j++; ?>
        
		  <?php }?>  
				  
			   </table>
      </div>
    </div>
	  
 </div> 
</div>
  </body>
</html>
