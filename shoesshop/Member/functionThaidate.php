<?php
function thainumDigit($num){
    return str_replace(array( '0' , '1' , '2' , '3' , '4' , '5' , '6' ,'7' , '8' , '9' ),array( "o" , "๑" , "๒" , "๓" , "๔" , "๕" , "๖" , "๗" , "๘" , "๙" ),$num);
}

function DThais($strDate){
		$strYear = date("Y",strtotime($strDate))+543;
		//$strMonth= date("n",strtotime($strDate));
		$strMonth= date("m",strtotime($strDate));
		$strDay= date("d",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		//$strMonthCut =Array('','มกราคม','กุมภาพันธุ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฏาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม');
		$strMonthThai=$strMonthCut[$strMonth];
		//return "$strDay $strMonthThai $strYear";
		return $strDay."/".$strMonth."/".$strYear;
	}

function DThai_long($strDate){
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		//$strHour= date("H",strtotime($strDate));
		//$strMinute= date("i",strtotime($strDate));
		//$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array('','มกราคม','กุมภาพันธุ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฏาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม');
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
	}

function FullThdate(){
	$thaiweek=array("วัน อาทิตย์","วัน จันทร์","วัน อังคาร","วัน พุธ","วัน พฤหัสบดี","วัน ศุกร์","วัน เสาร์");
	$thaimonth=array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
	echo $thaiweek[date("w")] ," ที่",date(" j "), $thaimonth[date(" m ")-1] , " พ.ศ. ",date(" Y ")+543;
}

function avg($arr) {
	$array_size = count($arr);
	$total = 0;
	for ($i = 0; $i < $array_size; $i++) {
		$total += $arr[$i];
	}
	$average = (float)($total / $array_size);
	return $average;
}


function time_ago($date,$granularity=2) {
    $date = strtotime($date);
    $difference = time() - $date;
    $periods = array('decade' => 315360000,
        'ปี' => 31536000,
        'เดือน' => 2628000,
        'สัปดาห์' => 604800, 
        'วัน' => 86400,
        'ชั่วโมง' => 3600,
        'นาที' => 60,
        'วินาที' => 1);
    if ($difference < 5) { // less than 5 seconds ago, let's say "just now"
        $retval = "ไม่กี่วินาทีที่แล้ว";
        return $retval;
    } else {                            
        foreach ($periods as $key => $value) {
            if ($difference >= $value) {
                $time = floor($difference/$value);
                $difference %= $value;
                $retval .= ($retval ? ' ' : '').$time.' ';
                //$retval .= (($time > 1) ? $key.'s' : $key);
                $retval .= (($time > 1) ? $key : $key);
                $granularity--;
            }
            if ($granularity == '0') { break; }
        }
        return $retval.' ที่ผ่านมา'; 
        //return ' posted '.$retval.' ago';      
    }
}


function dayColor(){
	$day=date('N');
	if($day=='1'){
		$color="#ffff00";
	}else if($day=='2'){
		$color="#ffccff";
	}else if($day=='3'){
		$color="#009900";
	}else if($day=='4'){
		$color="#ff9900";
	}else if($day=='5'){
		$color="#0066ff";
	}else if($day=='6'){
		$color="#800080";
	}else if($day=='7'){
		$color="#cc0000";
	}
	return $color;
}


	function convFormatDate($startdate){
        list($ddate, $mdate, $ydate ) = explode("-", $startdate);
        if (strlen($mdate)==1){
           $mdate = "0".$mdate;
        }
        if (strlen($ddate)==1){
            $ddate = "0".$ddate;
        }
        //$ydate = $ydate - 543;
        return $fdate = $ydate."-".$mdate."-".$ddate;
    }

	function convFormatDateSAP($startdate){
        list($ddate, $mdate, $ydate ) = explode(".", $startdate);
        if (strlen($mdate)==1){
           $mdate = "0".$mdate;
        }
        if (strlen($ddate)==1){
            $ddate = "0".$ddate;
        }
        if (strlen($ydate)==2){
            $ydate = "20".$ydate;
        }
        return $fdate = $ydate."-".$mdate."-".$ddate;
    }

	function DThai($startdate){
		list($ydate,$mdate,$ddate) = explode("-", $startdate);
        if (strlen($mdate)==1){
           $mdate = "0".$mdate;
        }
        if (strlen($ddate)==1){
            $ddate = "0".$ddate;
        }
        //$ydate = $ydate + 543;
        return $fdate = $ddate."-".$mdate."-".$ydate;
	}

	function DateTimeThai($strDate){
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear, เวลา $strHour:$strMinute น.";
	}

	function convDateTimeThai($strDate){
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("m",strtotime($strDate));
		$strDay= date("d",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		//$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		//$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay-$strMonth-$strYear $strHour:$strMinute";
	}



?>