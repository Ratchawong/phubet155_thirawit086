
<?php require_once('auth.php');?>
 <?php
 include('../connect2.php');
?>   
		
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<?php include('h.php');?>
    <?php include('datatable.php');?>
<script>

//<![CDATA[

$(function(){

 $('.imgx').hover(function(){

  var w = 200;

  var h = 220;

  var d = 600;//duration

  var imgx = $(this);

  $('.imgy').remove();

  var imgy = $('<img class="imgy" src="'+$(this).attr('src')+'"/>').appendTo('body');

  imgy.css({

   position: 'absolute',

   left: imgx.offset().left,

   top: imgx.offset().top,

   width: imgx.width(),

   height: imgx.height()

   }).mouseout(function(){

    $('.imgy').remove();

   }).click(function(){

    $('.imgy').remove();

   });

  imgy.animate({

   left: imgx.offset().left - (w/2),

   top: imgx.offset().top - (h/2),

   width: w+'px',

   height: h+'px'

  },d);

 },function(){});

});

//]]>

</script>
  </head>
  <body>
  <div class="container">
  <div class="row">
         <?php include('banner.php');?>
   </div>
	  <div class="row">
		 <div class="col-md-12">
          <?php include('navbar.php');?>
        </div>
      <div class="col-md-2">
        
        <?php include('menu.php');?>        	 
      </div>
 
    <div class="col-md-10">
      <div class="panel panel-primary class">
        <?php 
      include('../connect.php');
        $result = $db->prepare("SELECT * FROM `order` WHERE or_id");
        $result->execute();
        $row = $result->rowcount();

      ?>
    
          <div class="panel-heading" align="center" style="font-size: 20px;">คำสั่งซื้อ <font color="yellow" style="font:bold 22px 'Aleo';">[<?php echo $row;?>] รายการ </font></div>
        
           <div class="panel-body ">

   <table class="table-bordered " width="100%" >
          

    
 <!-- Content Row -->
     
              
            </div>       
         <table class="table table-bordered" id="resultTable" data-responsive="table" style="text-align: center;">
  <thead>
        <tr align="center">
              <td hidden="">วันที่สั่ง</td>
              <td>รหัสการสั่งซื้อ</td>
              <td>รูปสลิป</td>

              <td>ราคาราคาสุทธิ์</td>
              <td>สถานะชำระเงิน</td>
               <td>หมายเหตุ</td>

              
                        
                        
                        
        </tr>
  
</thead>
  <tbody>  
  <?php
   include('../connect1.php');
  
    $sql="SELECT * FROM `order`,`payment` WHERE order.or_id= payment.or_id order by order.or_id desc"  ;
    
     $result=mysqli_query($conn,$sql);
    while($data=mysqli_fetch_array($result, MYSQLI_ASSOC))
    { 
    ?>
      <tr class="record">
      
            <td hidden=""><?php echo $data['pay_id'] ;?> </td>
            <td hidden=""><?php echo $data['date'] ;?> </td>
            <td><?php echo $data['or_id'];?> </td> 

             <?php foreach(range(1,1) as $i):?>
          <td><center> <img src="../img/<?php echo $data["pay_pic"] ?>" width="50" height="50" class="imgx"></center></td>
             <?php endforeach;?>
             
            <td> <strong><?php echo $data['total'];?> </strong></td> 
            <td style="color:#060"><strong><?php echo $data['pay_status_bank'];?></strong> </td>  
          
     <td>                      
      <a href="viewOrder.php?id=<?php echo $data["or_id"];?> &adid=<?php echo "".$_SESSION["admin_id"].""; ?> &payid=<?php echo $data["pay_id"]; ?> "class="btn btn-primary"><i class="fa fa-shopping-cart"></i> ดูสินค้า</a></center> </td>
       
        
      <?php  
      }
       
      ?>
        <br>
    </tr>
      
            </table>   
            
       </tbody>
         </div>
         </div>
         </div>
                        </div>
 </div> 
</form>
 
</body>
</html>