<?php require_once('auth.php');?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<?php include('h.php');?>
    <?php include('datatable.php');?>
  
  </head>
  <body>
  <div class="container">
  <div class="row">
         <?php include('banner.php');?>
   </div>
	  <div class="row">
        <div class="col-md-12">
          <?php include('navbar.php');?>
        </div>
      </div>
  	<div class="row">
    	<div class="col-md-2">
        
        <?php include('menu.php');?>        	 
      </div>
      <div class="col-md-10">
		  <div class="panel panel-primary class">
			<?php 
			    include('../connect.php');
				$result = $db->prepare("select * from admin  where type_user ='MEMBER' order by admin_id");
				$result->execute();
				$row = $result->rowcount();

			?>
        	<div class="panel-heading" align="center" style="font-size: 20px;">สมาชิกทั้งหมด<font color="#FDFC06" style="font:bold 20px 'Aleo';">[<?php echo $row;?>] รายการ </font> </div>
			  
           <div class="panel-body ">
	<!-- ค้นหา -->
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script>    
		$(document).ready(function() {
			$('#example').DataTable();

		} );
	</script>
          <table class="table-bordered " width="100%"   id='example'  >
			 <thead>  
	 
          <tr>
		<th>ลำดับ</th>
        <th>ชื่อ-สกุล</th>
        <th>เบอร์</th>
        <th>ที่อยู่</th>
		<th>Email</th>
		<th>สถานะ</th>
		<th>แก้ไข</th>
            
          </tr>
        </thead>
         <?php
								include('../connect1.php');
							  
								$sql= "select * from admin  where type_user ='MEMBER' order by admin_id ";
								$result=mysqli_query($conn,$sql);
							//	for($i=0; $data=$result->fetch(); $i++ );{
	 									$i=1;
				while($data=mysqli_fetch_array($result, MYSQLI_ASSOC)){
			 ?>
            <tr>
          <td align="center" valign="top"><?php echo($i) ?></td>	   
		  <td> <?php echo $data['name']; ?></td> 
		  <td><?php echo $data['phone']; ?></td> 
		  <td><?php echo $data['address']; ?></td>
		  <td><?php echo $data['email']; ?></td>
		  <td><?php echo $data['type_user']; ?></td>
	          <td><center> 
		 <a rel="facebox" href="del_Member.php?id=<?php echo ($data['admin_id'])?>" onClick="return confirm('ยืนยันการลบ ?')"><img src="../img/icon-bin.png"class="action" title="ลบ"/></a>
            </tr>
            <?php	$i++; ?>
        
		  <?php }?>  
				  
			   </table>
			  
		
      </div>
    </div>


		  
 </div> 
</div>
  </body>
</html>
