<?php include('h.php');?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
    <style type="text/css">
        input[type=number]{
          width:40px;
          text-align:center;
          color:red;
          font-weight:600;
        }
    </style>
  </head>
  <body >
    <div class="container">
      <div class="row">
        <?php include('banner.php');?>
      </div>
      <div class="row">
        <div class="col-md-12">
          <?php include('navbar.php');?>
        </div>
      </div>
    </div>
    <!--start show  detail-->
    <div class="container">
      <div class="row">
        <!-- categories-->
       <div class="col-md-2">
        
        <?php include('menu.php');?>        	 
      </div>
        <!-- detail-->
     <div class="col-md-10">
		  <div class="panel panel-primary class">
			  <div class="col-md-10">
            </div>
			
			  
        	<div class="panel-heading" align="center" style="font-size: 20px;">เพิ่มสินค้า</div
			  <br>
			
	 
      <div class="panel-body " >	   

<form  action="insertProduct.php" name="frmAdd" id="frmAdd" method="post" enctype="multipart/form-data"  >

  <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td colspan="3" align="center">&nbsp;</td>
    </tr>
    <tr>
	
	 <div class="panel-body">
             
                <label class="col-sm-3 control-label"> ชื่อสินค้า :  <span class="starrequired">*</span></label>
        		<div class="col-sm-9"><input class="form-control" type="text" name="p_name"  /><br> </div> 
   				
				<label class="col-sm-3 control-label"> Size : <span class="starrequired">*</span></label>
			    <div class="col-sm-9"> <select class="form-control"  name="p_size"  >
				<option value="">-- กรุณาเลือก --</option>
				<option value="36">36</option>
				<option value="37">37</option>
				<option value="38">38</option>
				<option value="39">39</option>
				<option value="40">40</option>
				<option value="41">41</option>
				<option value="42">42</option>
				<option value="43">43</option>
			  	</select><br></div> 
		 
		 		<label class="col-sm-3 control-label"> ราคา :  <span class="starrequired">*</span></label>
			    <div class="col-sm-9"><input   class="form-control"  name="p_price"   /> <br> </div>
		 	<label class="col-sm-3 control-label">รูปสินค้า :  <span class="starrequired">*</span></label>
			    <div class="col-sm-9"><input type="file"  class="form-control"  name="p_pic"   /> <br> </div>
	 	  			
					 
		</div>			
				</label>
</div>
							
  <div class="form-group">
      <div class="col-sm-offset-3 col-sm-9">
      	<button class="btn btn-primary" type="submit"  name="btn-upload">ตกลง</button>
		<button  type="reset" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
		
      </div>
    </div>
     
</div> 
	  </table>
 
	</form>
    </div>
    </div >


 </div> 
</div>
	  


  </body>
</html>


 
 