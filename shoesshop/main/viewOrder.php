
<?php require_once('auth.php');?>
 <?php
 include('../connect2.php');
?>   
		
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<?php include('h.php');?>
    <?php include('datatable.php');?>
<script>

//<![CDATA[

$(function(){

 $('.imgx').hover(function(){

  var w = 200;

  var h = 220;

  var d = 600;//duration

  var imgx = $(this);

  $('.imgy').remove();

  var imgy = $('<img class="imgy" src="'+$(this).attr('src')+'"/>').appendTo('body');

  imgy.css({

   position: 'absolute',

   left: imgx.offset().left,

   top: imgx.offset().top,

   width: imgx.width(),

   height: imgx.height()

   }).mouseout(function(){

    $('.imgy').remove();

   }).click(function(){

    $('.imgy').remove();

   });

  imgy.animate({

   left: imgx.offset().left - (w/2),

   top: imgx.offset().top - (h/2),

   width: w+'px',

   height: h+'px'

  },d);

 },function(){});

});

//]]>

</script>
  </head>
  <body>
  <div class="container">
  <div class="row">
         <?php include('banner.php');?>
   </div>
	  <div class="row">
		 <div class="col-md-12">
          <?php include('navbar.php');?>
        </div>
      <div class="col-md-2">
        
        <?php include('menu.php');?>        	 
      </div>
 
    <div class="col-md-10">
      <div class="panel panel-primary class">
      
    
          <div class="panel-heading" align="center" style="font-size: 20px;">รายการสินค้า </div>
        
           <div class="panel-body ">

   <table class="table-bordered " width="100%" >
          

    
 <!-- Content Row -->
     
              
            </div>       
         <table class="table table-bordered" id="resultTable" data-responsive="table" style="text-align: center;">
  <thead>
        <tr align="center">
              <td hidden="">วันที่สั่ง</td>
              <th width="80" align="center" class="header"> รหัสสั่งซื้อ </td>
              <th width="150" class="header"> ชื่อ - นามสกุล </td>
              <th width="80" class="header"> วันที่สั่ง </td>
              <th width="80" class="header"> รูปสินค้า </td>
              <th width="80" class="header"> ชื่อสินค้า </td>
              <th width="80" class="header"> ขนาด </td>
              <th width="80" class="header"> ราคา </td>
              <th width="80" class="header"> จำนวน </td>
             
              
                        
                        
                        
        </tr>
  
</thead>
  <tbody>  
  <?php
  include('../connect.php');
 $id = $_GET['id'];
 $uid = $_GET['adid'];
 $result = $db->prepare("SELECT * FROM `order`,`admin`,`detailorder`,`product` 
    WHERE admin.admin_id= order.admin_id
    and product.p_id = detailorder.p_id
    and order.or_id = detailorder.OrderID
    and detailorder.OrderID = '$id'
    and order.admin_id ");
    $result->bindParam(':userid', $id);
    $result->execute();
    for($i=0; $row = $result->fetch(); $i++){ 
  
      ?>
      <tr class="record">
      <td align="center"><?php echo $row['or_id'];?></td>
            <td align="center"><?php echo $row['name'];?><br><?php echo $row['address'];?>
              
            </td>
            <td align="center"><?php echo  date("d-m-Y") ; ?> </td>
            <!--hidden--><td hidden >P000-<?php echo $row['_id']; ?></td>
            <td align="center"><img src="../img/<?php echo $row['p_pic'];?> " width="70" height="70"></td>
           
      <td align="center"><?php echo $row['p_name']; ?></td>
            <td align="center"><?php echo $row['p_size']; ?></td> 
  <td align="center"><?php echo $row['de_price'];?> บาท</td> 
      <td align="center"><?php echo $row['de_amount']; ?> ชิ้น</td>
           
        
      <?php  
      }
       
      ?>
        <br>
    </tr>
      
            </table>   
            
       </tbody>
         </div>
         </div>
         </div>
                        </div>
 </div> 
</form>
 
</body>
</html>