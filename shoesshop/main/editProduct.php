<?php require_once('auth.php');?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<?php include('h.php');?>
    <?php include('datatable.php');?>

	  
	  <script>

//<![CDATA[

$(function(){

 $('.imgx').hover(function(){

  var w = 200;

  var h = 220;

  var d = 600;//duration

  var imgx = $(this);

  $('.imgy').remove();

  var imgy = $('<img class="imgy" src="'+$(this).attr('src')+'"/>').appendTo('body');

  imgy.css({

   position: 'absolute',

   left: imgx.offset().left,

   top: imgx.offset().top,

   width: imgx.width(),

   height: imgx.height()

   }).mouseout(function(){

    $('.imgy').remove();

   }).click(function(){

    $('.imgy').remove();

   });

  imgy.animate({

   left: imgx.offset().left - (w/2),

   top: imgx.offset().top - (h/2),

   width: w+'px',

   height: h+'px'

  },d);

 },function(){});

});

//]]>

</script>
	  
	  
  </head>
  <body>
  <div class="container">
  <div class="row">
         <?php include('banner.php');?>
   </div>
	  <div class="row">
        <div class="col-md-12">
          <?php include('navbar.php');?>
        </div>
      </div>
  	<div class="row">
    	<div class="col-md-2">
        
        <?php include('menu.php');?>        	 
      </div>
      <div class="col-md-10">
		  <div class="panel panel-primary class">
			  <div class="col-md-10">
            </div>
			
			  
			  <div class="panel-heading" align="center" style="font-size: 20px;">แก้ไขสินค้า</div>
			  <br>
			
	 
      <div class="panel-body " >	   

<form  action="updateProduct.php" name="frmAdd" id="frmAdd" method="post" enctype="multipart/form-data"  >
 <?php  
// ติดต่อฐานข้อมูล  
			include('../connect.php');
			$id=$_GET['id'];
			$result = $db->prepare("SELECT * FROM product WHERE p_id= :pid");
			$result->bindParam(':pid', $id);
			$result->execute();
			for($i=0; $data = $result->fetch(); $i++){
		?>
  <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td colspan="3" align="center">&nbsp;</td>
    </tr>
    <tr>
	
	 <div class="panel-body">
		 
				 <div hidden="">
				  <label class="col-sm-3 control-label"> รหัส :  <span class="starrequired">*</span></label>
				 <div class="col-sm-9"><input class="form-control" type="text" value="<?php echo($data['p_id'])?>" name="p_id"  /><br> </div> 
					</div>
             
                <label class="col-sm-3 control-label"> ชื่อสินค้า :  <span class="starrequired">*</span></label>
        		<div class="col-sm-9"><input class="form-control" type="text" name="p_name" value="<?php echo($data['p_name'])?> " /><br> </div> 
   				
				<label class="col-sm-3 control-label"> Size : <span class="starrequired">*</span></label>
			    <div class="col-sm-9"> <select class="form-control"  name="p_size"  >
				<option value="<?php echo($data['p_size'])?>"><?php echo($data['p_size'])?> </option>
				<option value="36">36</option>
				<option value="37">37</option>
				<option value="38">38</option>
				<option value="39">39</option>
				<option value="40">40</option>
				<option value="41">41</option>
				<option value="42">42</option>
				<option value="43">43</option>
			  	</select><br></div> 
		 
		 		<label class="col-sm-3 control-label"> ราคา :  <span class="starrequired">*</span></label>
			    <div class="col-sm-9"><input   class="form-control"  name="p_price"  value="<?php echo($data['p_price'])?> " /> <br> </div>
		 		<label class="col-sm-3 control-label">รูปสินค้า :  <span class="starrequired">*</span></label>
			    <div class="col-sm-9"><input type="file"  class="form-control"  name="p_pic" /> <br> </div>
	  
	  			
					 
		</div>			
				<br>	</label>
	<br><br></div>
					
			

  <div class="form-group">
      <div class="col-sm-offset-3 col-sm-9">
      	<button class="btn btn-primary" type="submit"  name="btn-upload">ตกลง</button>
		<button  type="reset" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
		
      </div>
    </div>
     
</div> 
	  </table>
 
	</form>
				  <?php
}
?>  
		  
 </div> 
</div>
  </body>
</html>
