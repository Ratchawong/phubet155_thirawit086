

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<?php include('h.php');?>
  </head>
	<?php
	//Start session
	//session_start();
	
	//Unset the variables stored in session
	//ยกเลิก/ลบค่า Delete Session
		
	unset($_SESSION['admin_id']);
	unset($_SESSION['admin_name']);
	unset($_SESSION['password']);
	unset($_SESSION['username']);
	unset($_SESSION['type_user']);
?>

  <body>
  <div class="container">
  	<div class="row">
         <?php include('banner.php');?>
   </div>
  	<div class="row">
    	<div class="col-md-12">
        	<?php include('navbar.php');?>
        </div>
    </div>
        </div>
        <div class="row" style="padding-top:100px">
        <div class="col-md-4"></div>
    	<div class="col-md-4" style="background-color:#f4f4f4">
                  <h3 align="center">
                  <span class="glyphicon glyphicon-lock"> </span>
                   Login</h3>
			<?php
if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR']) && count($_SESSION['ERRMSG_ARR']) >0 ) {
	foreach($_SESSION['ERRMSG_ARR'] as $msg) {
		echo '<div style="color: red; text-align: center;">',$msg,'</div><br>'; 
	}
	unset($_SESSION['ERRMSG_ARR']);
}
?>
                  <form ACTION="checklogin.php"  name="formlogin" method="POST" id="login" class="form-horizontal">
                    <div class="form-group">
                      <div class="col-sm-12">
                        <input  name="username" type="text" required class="form-control"   placeholder="Username" />
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-12">
                        <input name="password" type="password" required class="form-control"   placeholder="Password" />
                      </div>
                    </div>
                    <div class="form-group" align="center" >
                      <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary" id="btn">
                        <span class="glyphicon glyphicon-log-in"> </span>
                         Login </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
        </div>
    </div>
 </div>

 
 
 
 
   
  </body>
</html>
