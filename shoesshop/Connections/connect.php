<?php
/* Database config */
$db_host		= 'localhost';
$db_user		= 'root';
$db_pass		= '';
$db_database	= 'research'; 

/* End config */

$db = new PDO('mysql:host='.$db_host.';dbname='.$db_database, $db_user, $db_pass);
//กำหนดให้เตือนข้อผิดพลาดที่เกี่ยวกับการติดต่อฐานข้อมูล
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$db->exec ( "SET NAMES \"utf8\"" );


//set charset for thai
//mysqli_set_charset($conn, "utf8");
 mysql_query("SET NAMES UTF8");
?>